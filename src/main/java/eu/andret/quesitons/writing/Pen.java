package eu.andret.quesitons.writing;

public non-sealed class Pen extends WritingTool {
	public Pen(final String name) {
		super(name);
	}
}
