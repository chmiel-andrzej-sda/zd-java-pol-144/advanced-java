package eu.andret.quesitons.writing;

import eu.andret.quesitons.writing.Feather;

public non-sealed class GooseFeather extends Feather {
	public GooseFeather(final String name) {
		super(name);
	}
}
