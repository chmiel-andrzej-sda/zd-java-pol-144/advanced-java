package eu.andret.quesitons.writing;

import java.util.Objects;

/**
 * The WritingTool class represents a generic writing tool.
 * It is an abstract class that can be extended by specific writing tools such as Feather, Pen, and Pencil.
 */
public sealed class WritingTool permits Feather, Pen, Pencil {
	private final String name;

	public WritingTool(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final WritingTool that = (WritingTool) o;
		return Objects.equals(getName(), that.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName());
	}

	@Override
	public String toString() {
		return "WritingTool{" +
				"name='" + name + '\'' +
				'}';
	}
}
