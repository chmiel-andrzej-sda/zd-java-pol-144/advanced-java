package eu.andret.quesitons.writing;

/**
 * The Feather class represents a feather writing tool.
 * It is a sealed class and is the base class for different types
 * of feathers, such as the GooseFeather.
 */
public sealed class Feather extends WritingTool permits GooseFeather {
	public Feather(final String name) {
		super(name);
	}
}
