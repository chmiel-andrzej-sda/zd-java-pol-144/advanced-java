package eu.andret.quesitons.functional;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		// Function<Integer, Integer>
		IntUnaryOperator integerIntegerFunction = new IntUnaryOperator() {
			@Override
			public int applyAsInt(int integer) {
				int result = 0;
				for (int i = 0; i < integer; i++) {
					result += i;
				}
				return result;
			}
		};
		System.out.println(integerIntegerFunction.applyAsInt(10));

		Random random = new Random();
		Supplier<Integer> supplier = new Supplier<Integer>() {
			@Override
			public Integer get() {
				return random.nextInt(11);
			}
		};
		System.out.println(supplier.get());

		Consumer<String> consumer = new Consumer<String>() {
			@Override
			public void accept(String s) {
				for (int i = 0; i < s.toCharArray().length; i++) {
					System.out.println(s.charAt(i));
				}
			}
		};
		consumer.accept("test");

		Predicate<Integer> isPrime = new Predicate<Integer>() {
			@Override
			public boolean test(Integer integer) {
				for (int i = 2; i < integer / 2; i++) {
					if (integer % i == 0) {
						return false;
					}
				}
				return true;
			}
		};
		System.out.println(isPrime.test(21));

		Stream.of(8, 3, 7, 11, 28, 0, -4, 1)
				.filter(integer -> integer % 2 == 0)
				.map(integer -> integer * integer)
				.forEach(Main::saveToFile);
	}

	public static void saveToFile(final int value) {
		System.out.println("Saving " + value + " to file!");
	}
}
