package eu.andret.quesitons.reflection;

public class MyTestClass {
	@Priority(1)
	public void publicMethod() {
		System.out.println("public!");
	}

	@Priority(value = 2, description = "test")
	void packagePrivateMethod() {
		System.out.println("package-private!");
	}

	@Ignore
	@Priority(description = "hello world")
	protected void protectedMethod() {
		System.out.println("protected!");
	}

	@Priority
	private void privateMethod() {
		System.out.println("private!");
	}
}
