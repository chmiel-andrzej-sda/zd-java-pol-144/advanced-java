package eu.andret.quesitons.reflection;

import java.util.Objects;

public class Person {
	private final String fullName;
	private double height;

	public Person(final String fullName, final double height) {
		this.fullName = fullName;
		this.height = height;
	}

	@MyAnnotation
	public String getFullName() {
		return fullName;
	}

	@MyAnnotation
	public double getHeight() {
		return height;
	}

	public void setHeight(final double height) {
		this.height = height;
	}

	private void doIt() {
		System.out.println("I'm doing it");
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Person person = (Person) o;
		return Double.compare(getHeight(), person.getHeight()) == 0 && Objects.equals(getFullName(), person.getFullName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getFullName(), getHeight());
	}

	@Override
	public String toString() {
		return "Person{" +
				"fullName='" + fullName + '\'' +
				", height=" + height +
				'}';
	}
}
