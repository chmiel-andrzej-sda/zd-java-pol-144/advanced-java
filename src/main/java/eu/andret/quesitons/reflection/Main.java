package eu.andret.quesitons.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ReflectPermission;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
	public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, InstantiationException {
		Person p = new Person("Abc Def", 190);
		Method[] declaredMethods1 = Person.class.getDeclaredMethods();
		Method[] declaredMethods = declaredMethods1;
		System.out.println(Arrays.toString(declaredMethods));
		Method doIt = Person.class.getDeclaredMethod("doIt");
		System.out.println(doIt);
		System.out.println(doIt.getReturnType());
		System.out.println(Arrays.toString(doIt.getParameters()));
		System.out.println(doIt.getName());
		doIt.setAccessible(true);
		doIt.invoke(p);
		System.out.println(Arrays.toString(Person.class.getDeclaredMethod("toString").getDeclaredAnnotations()));

		Field height = Person.class.getDeclaredField("height");
		height.setAccessible(true);
		System.out.println(height.getDouble(p));

		height.setDouble(p, -10);

		System.out.println(p.getHeight());

		Field fullName = Person.class.getDeclaredField("fullName");
		fullName.setAccessible(true);
		System.out.println(fullName.get(p));

		fullName.set(p, "test");

		System.out.println(p.getFullName());

		System.out.println(Arrays.toString(Weekday.class.getDeclaredConstructors()));

		Constructor<Weekday> declaredConstructor = Weekday.class.getDeclaredConstructor(String.class, int.class);
		System.out.println(declaredConstructor);
		declaredConstructor.setAccessible(true);
//		Weekday weekday = declaredConstructor.newInstance(); // -> exception
		Arrays.stream(declaredMethods1)
				.filter(m -> m.isAnnotationPresent(MyAnnotation.class))
				.map(m -> {
					System.out.println(m.getDeclaredAnnotation(MyAnnotation.class).test());
					try {
						return m.invoke(p);
					} catch (ReflectiveOperationException e) {
						throw new RuntimeException(e);
					}
				})
				.forEach(System.out::println);

		MyTestClass myTestClass = new MyTestClass();
		Arrays.stream(MyTestClass.class.getDeclaredMethods())
				.filter(method -> !method.isAnnotationPresent(Ignore.class))
				.peek(System.out::println)
				.sorted(Comparator.comparingInt(o -> o.getDeclaredAnnotation(Priority.class).value()))
				.forEachOrdered(method -> {
					method.setAccessible(true);
					System.out.println(method.getDeclaredAnnotation(Priority.class).description());
					try {
						method.invoke(myTestClass);
					} catch (ReflectiveOperationException e) {
						throw new RuntimeException(e);
					}
				});
	}
}

enum Weekday {
	MONDAY,
	TUESDAY
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
@interface MyAnnotation {
	Class<?>[] test() default Person.class;
}
