package eu.andret.quesitons.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class MainClass {
	public static void main(String[] args) throws IOException {
		File file = new File("./src/main/java/eu/andret/questions/Main.java");
		File[] files = file.listFiles();
		System.out.println(Arrays.toString(files));

		Path path = Paths.get("src", "main", "java", "eu", "andret", "quesitons", "Main.java");
		List<String> strings = Files.readAllLines(path);
		strings.forEach(System.out::println);

		try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		// "test" istnieje
		// createDirectory(Paths.get("test", "abc"));
		// createDirectories(Paths.get("test", "abc"));


		// createDirectory(Paths.get("test", "abc", "xyz")); // -> error
		// createDirectories(Paths.get("test", "abc", "xyz")); // -> works

//		Files.createDirectory(Paths.get("files"));

		Path path1 = Paths.get("files", "test1.txt");
		try (PrintWriter writer = new PrintWriter(path1.toFile())) {
			writer.println("Hello world");
		}

		Path path2 = Paths.get("files", "test2.txt");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path2.toFile()))) {
			writer.write("Hello world");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
